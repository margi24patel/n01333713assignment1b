﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace SuperTest
{
    public class Client
    {
        private string clientFirstName;
        private string clientLastName;
        private string clientStreetAddress;
        private string clientCity;
        private string clientPSCode;
        private string clientCountryName;
        private string clientEmail;
        private string clientPhone;

        public Client()
        {
        }

        public string ClientFirstName
        {
            get { return clientFirstName; }
            set { clientFirstName = value; }
        }


        public string ClientLastName
        {
            get { return clientLastName; }
            set { clientLastName = value; }
        }


        public string ClientStreetAddress
        {
            get { return clientStreetAddress; }
            set { clientStreetAddress = value; }
        }
    

        public string ClientCity
        {
            get { return clientCity; }
            set { clientCity = value; }
        }


        public string ClientPSCode
        {
            get { return clientPSCode; }
            set { clientPSCode = value; }
        }
    

        public string ClientCountryName
        {
            get { return clientCountryName; }
            set { clientCountryName = value; }
        }
    

        public string ClientEmail
        {
            get { return clientEmail; }
            set { clientEmail = value; }
        }
    

        public string ClientPhone
        {
            get { return clientPhone; }
            set { clientPhone = value; }
        }
    
    }

}
