﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperTest
{
    public class Invoice
    {
        public Client client;

        public Booking booking;

        public Property property;


        public Invoice(Client c, Booking b, Property p)
        {
            client = c;

            booking = b;

            property = p;
        }





        public string GenerateInvoice()
        {
            string invoice = "Invoice : <br>";
            invoice += "Your total Bill is :" + CalculateInvoice().ToString() + "<br>";
            invoice += "FirstName: " + client.ClientFirstName + "<br>";
            invoice += "LastName: " + client.ClientLastName + "<br>";
            invoice += "StreetAddress: " + client.ClientStreetAddress + "<br>";
            invoice += "City Name,State: " + client.ClientCity + "<br>";
            invoice += "Postal-Code: " + client.ClientPSCode + "<br>";
            invoice += "Country Name: " + client.ClientCountryName + "<br>";
            invoice += "Email-ID: " + client.ClientEmail + "<br>";
            invoice += "Phone Number: " + client.ClientPhone + "<br>";


            invoice += "Check-In Date:" + booking.checkinDate.ToString("dd/MM/yyyy") + "<br>";
            invoice += "Check-Out Date:" + booking.checkoutDate.ToString("dd/MM/yyyy") + "<br>";
            invoice += "No. of Person: " + booking.noPerson.ToString() + "<br>";
           

            invoice += "RoomType: " + property.roomType + "<br>";
            invoice += "Property Type: " + property.propertyType + "<br>";
            invoice += "Facility Type: " + String.Join(" , ", property.facility.ToArray()) + "<br>"; 

            return invoice;
        }




       

        public float CalculateInvoice()
        {


            //Here, this part for Room Type = "Delux"
            float total = 0.00f;
            if(property.roomType == "Delux" && property.propertyType == "Apartments")
            {
                total = 10.50f;
            }

            else if(property.roomType == "Delux" && property.propertyType == "GuestHouses")
            {
                total = 20.60f;
            }

            else if (property.roomType == "Delux" && property.propertyType == "HomeStays")
            {
                total = 30.70f;
            }

            else if (property.roomType == "Delux" && property.propertyType == "Vacation")
            {
                total = 40.80f;
            }

            //Here, this part for RoomType = "Super Delux"

            else if (property.roomType == "Super Delux" && property.propertyType == "Apartments")
            {
                total = 50.90f;
            }

            else if (property.roomType == "Super Delux" && property.propertyType == "GuestHouses")
            {
                total = 60.25f;
            }

            else if (property.roomType == "Super Delux" && property.propertyType == "HomeStays")
            {
                total = 70.48f;
            }

            else if (property.roomType == "Super Delux" && property.propertyType == "Vacation")
            {
                total = 80.99f;
            }

            ////Here, this part for RoomType = "Family"

            else if (property.roomType == "Family" && property.propertyType == "Apartments")
            {
                total = 90.89f;
            }

            else if (property.roomType == "Family" && property.propertyType == "GuestHouses")
            {
                total = 100.84f;
            }

            else if (property.roomType == "Family" && property.propertyType == "HomeStays")
            {
                total = 110.44f;
            }

            else if (property.roomType == "Family" && property.propertyType == "Vacation")
            {
                total = 120.32f;
            }



            return total;
        }
    }
}
