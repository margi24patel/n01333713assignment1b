﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="test.aspx.cs" Inherits="SuperTest.test" enableEventValidation="false" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>test</title>
</head>
<body>
       
	<form id="form1" runat="server">
            <div>
                
                <h1>Hotel Reservation Form</h1>
            <asp:Label id="lblName" runat="server" text="Name"></asp:Label>
            <br>
           
            <asp:TextBox id="txtfname" runat="server" placeholder="FirstName"></asp:TextBox>
            <asp:RequiredFieldValidator id="Requiredfieldvalidatorfname" runat="server" ErrorMessage="Please enter First Name" ControlToValidate="txtfname" ></asp:RequiredFieldValidator>
            <br>
            
            <asp:TextBox id="txtlname" runat="server" placeholder="LastName"></asp:TextBox>
            <asp:RequiredFieldValidator id="Requiredfieldvalidatorlname" runat="server" ErrorMessage="Please enter Last Name" ControlToValidate="txtlname"></asp:RequiredFieldValidator>
            <br>
            
            <asp:Label id="lbladdress" runat="server" text="Address"></asp:Label>
            <br>
            
            <asp:TextBox id="txtstreetadd" runat="server" placeholder="Street Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter StreetAddres" ControlToValidate="txtstreetadd" id="Requiredfieldvalidatorstreetadd"></asp:RequiredFieldValidator>
            <br>
            
            <asp:TextBox id="txtcity" runat="server" placeholder="City Name,State"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter CityName" ControlToValidate="txtcity" id="Requiredfieldvalidatorcity"></asp:RequiredFieldValidator>
            <br>
            
             <asp:TextBox id="txtpscode" runat="server" placeholder="Postal-Code"></asp:TextBox>
             <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Postal-Code" ControlToValidate="txtpscode" id="Requiredfieldvalidatorpscode"></asp:RequiredFieldValidator>
             <br>
	        
             <asp:Label id="lblcountryname" runat="server" text="Country Name:"></asp:Label>
            
             <asp:DropDownList id="country" runat="server">
               <asp:ListItem Value="India" Text="India"></asp:ListItem>
                <asp:ListItem Value="Canada" Text="Canada"></asp:ListItem>
                <asp:ListItem Value="USA" Text="USA"></asp:ListItem>
                <asp:ListItem Value="Brazil" Text="Brazil"></asp:ListItem>
                <asp:ListItem Value="UK" Text="UK"></asp:ListItem>
                <asp:ListItem Value="Australia" Text="Australia"></asp:ListItem>
                <asp:ListItem Value="New Zeland" Text="New Zeland"></asp:ListItem>
               </asp:DropDownList>
             <br>
             <br>
        
            <asp:Label id="clientemail" runat="server" text="Email-ID:"></asp:Label>
            <asp:TextBox id="txtemail" runat="server" placeholder="Email"></asp:TextBox>
            <asp:RequiredFieldValidator id="Requiredfieldvalidatortxtemail" runat="server" ErrorMessage="Please enter an Email" ControlToValidate="txtemail"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator id="Regularfieldvalidatortxtemail" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtemail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br>
            <br>
            
            <asp:Label id="clientphone" runat="server" text="Phone Number:"></asp:Label>
            <asp:TextBox id="txtphone" runat="server" placeholder="Phone"></asp:TextBox>
            <asp:RequiredFieldValidator  id="Requiredfieldvalidatortxtphone" runat="server" ErrorMessage="Please enter a Phone Number" ControlToValidate="txtphone"></asp:RequiredFieldValidator>
            
            <asp:RegularExpressionValidator id="RegularexpressionvalidatorPhone" runat="server" ErrorMessage="Enter valid Phone number" ControlToValidate="txtphone" ValidationExpression= "^([\(]{1}[0-9]{3}[\)]{1}[\.| |\-]{0,1}|^[0-9]{3}[\.|\-| ]?)?[0-9]{3}(\.|\-| )?[0-9]{4}$"></asp:RegularExpressionValidator>
            <br>
                
             <asp:Label id="clientcheckin" runat="server" text="Check-In Date:"></asp:Label>
             <asp:TextBox id="txtcheckin" runat="server" placeholder="DD/MM/YYYY"></asp:TextBox>
             <asp:RequiredFieldValidator id="Requiredfieldvalidatortxtcheckin" runat="server"  ErrorMessage="Please enter valid Date" ControlToValidate="txtcheckin"></asp:RequiredFieldValidator>
             
            <asp:RegularExpressionValidator id="Regularexpressionvalidatortxtcheckin" ErrorMessage="DateFormat should be dd/mm/yyyy" ControlToValidate="txtcheckin" runat="server" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/]\d{4}"/>
            
             <br>
                
               <!-- i addeed check-in and check-out for date -->
                
             <asp:Label id="clientcheckout" runat="server" text="Check-Out Date:"></asp:Label>
             <asp:TextBox id="txtcheckout" runat="server" placeholder="DD/MM/YYYY"></asp:TextBox>
             <asp:RequiredFieldValidator id="Requiredfieldvalidatortxtcheckout" runat="server"  ErrorMessage="Please enter valid Date" ControlToValidate="txtcheckout"></asp:RequiredFieldValidator>
             
            <asp:RegularExpressionValidator id="Regularexpressionvalidatortxtcheckout" ErrorMessage="DateFormat should be dd/mm/yyyy" ControlToValidate="txtcheckout" runat="server" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/]\d{4}"/>
            
             <br>
   
                
                
            <asp:Label id="person" runat="server" text="No. of Person:"></asp:Label>
            <asp:TextBox runat="server" id="txtperson" placeholder="No. Of Person"></asp:TextBox>  
            <asp:RequiredFieldValidator id="validatortxtperson" runat="server" ErrorMessage="Please enter No. of Person" ControlToValidate="txtperson"></asp:RequiredFieldValidator>
            <asp:RangeValidator Enable="false" id="Rangevalidatortxtchktime" runat="server" ControlToValidate="txtperson" MinimumValue="1" Type="Integer" MaximumValue="4" ErrorMessage="No. of Person must be between 1 to 4"></asp:RangeValidator>
            <br>
            <br>
            <br>
            
            <asp:Label id="lblpaymenttype" runat="server" text="Payment Type:" ></asp:Label>
            
            <asp:RadioButtonList id="paymenttype" runat="server" >
                    
                <asp:ListItem Text="Check" runat="server" ></asp:ListItem>
                <asp:ListItem Text="Paypal" runat="server"></asp:ListItem>
            </asp:RadioButtonList>
               
                
           
          
            <asp:CustomValidator id="Customvalidatorpaymenttype" OnServerValidate="PaymentChoice_Validator" ControlToValidate="paymenttype" ErrorMessage="Paypal through only!!" runat="server"></asp:CustomValidator>
            <br>
            <br>
             
              <!--i edit the rooptype and property type in dropdown list-->
                
            <asp:Label id="lblroomtype" runat="server" text="Room Type:" ></asp:Label>    
            <asp:DropDownList runat="server" id="property_qualityDelux">
              <asp:ListItem Value="Delux" Text="Delux"></asp:ListItem>
              <asp:ListItem Value="Super Delux" Text="Super Delux"></asp:ListItem>
              <asp:ListItem Value="Family" Text="Family"></asp:ListItem>
                </asp:DropDownList>
            <br>
            <br>
                
               
                
                
            <asp:Label id="lblpropertytype" runat="server" text="Property Type:" ></asp:Label>
           
            <asp:DropDownList runat="server" id="property_quality">
            <asp:ListItem Value="Apartments" Text="Apartments"></asp:ListItem>
            <asp:ListItem Value="GuestHouses" Text="GuestHouses"></asp:ListItem>
            <asp:ListItem Value="HomeStays" Text="HomeStays"></asp:ListItem>
            <asp:ListItem Value="Vacation Homes" Text="Vacation Homes"></asp:ListItem>        
            </asp:DropDownList>  
            <br>
            <br>
                    
            
            
            <div id="extraAddFacility" runat="server">
            <asp:Label id="lblfacility" runat="server" text="Facility:"></asp:Label>
            <asp:CheckBox runat="server" id="Fc" Text="Fitness Center"></asp:CheckBox>
            <asp:CheckBox runat="server" id="Ns" Text="Non-Smoking Rooms"></asp:CheckBox>
            <!--<asp:CheckBox runat="server" id="WiFi" Text="Free WiFi"></asp:CheckBox>
            <asp:CheckBox runat="server" id="Pa" Text="Parking"></asp:CheckBox>-->
            <asp:CheckBox runat="server" id="Re" Text="Restaurant"></asp:CheckBox>
            <asp:CheckBox runat="server" id="As" Text="Airport Shuttle"></asp:CheckBox>
            <asp:CheckBox runat="server" id="Dg" Text="Facilities for Disabled Guests"></asp:CheckBox>    
                    
            </div>
            <br><br>
            
            <asp:Label id="star" runat="server" Text="Star Rating:"></asp:Label>
            <asp:RadioButton runat="server" id="S1" Text="1 Star" GroupName="Star"></asp:RadioButton>
            <asp:RadioButton runat="server" id="S2" Text="2 Star" GroupName="Star"></asp:RadioButton>
            <asp:RadioButton runat="server" id="S3" Text="3 Star" GroupName="Star"></asp:RadioButton>
            <asp:RadioButton runat="server" id="S4" Text="4 Star" GroupName="Star"></asp:RadioButton>
            <asp:RadioButton runat="server" id="S5" Text="5 Star" GroupName="Star"></asp:RadioButton>
              
            <br>   
            <br> 
                
          
              
             <asp:Button id="btnsubmit" runat="server" Text="Submit"  OnClick="Invoice" />
             <br>
             
       
                
             <div>
                 <asp:ValidationSummary id="validationsummary1" runat="server" HeaderText="Please fill up following Details :"
                  ShowMessageBox="false" DisplayMode="BulletList" ShowSummary="true" BackColor="SkyBlue" Width="400" ForeColor="Black"
                   Font-Size="small" Font-Bold="true" />
                    
             </div>
               
              <div id="msg" runat="server"></div>
                
              <div id="ClientInfo" runat="server"></div>
                
              <div id="BookingInfo" runat="server"></div>
                    
              <div id="PropertyInfo" runat="server"></div>  
               
              <div id="InvoiceInfo" runat="server"></div>
          
              <footer id="footer" runat="server"></footer>
           
            
            </div> 
            
           
        </form>
</body>
</html>
