﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperTest
{
    public class Booking
    {
        public int noPerson;
        public DateTime checkinDate;
        public DateTime checkoutDate;
        public Booking(int p ,DateTime d, DateTime o)
        {
            noPerson = p;
            checkinDate = d;
            checkoutDate = o;
        }
    }
}
