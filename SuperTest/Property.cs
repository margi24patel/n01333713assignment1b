﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperTest
{
    public class Property
    {
        public string roomType;

        public string propertyType;

        public List<string> facility;

        public Property(string rt,string pt,List<string> fc)
        {
            roomType = rt;

            propertyType = pt;

            facility = fc;
        }
    }
}
