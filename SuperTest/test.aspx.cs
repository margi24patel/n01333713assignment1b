﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace SuperTest
{

    public partial class test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

           // txtfname.Text = "Margi";
            
            //txtlname.Text = "Patel";
            
        }

        /* REFERENCE : As per Cristine's code*/
       
        protected void PaymentChoice_Validator(object source, ServerValidateEventArgs args)
        {
            if(paymenttype.SelectedValue == "Paypal")
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void Invoice(object sender,EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }




            /* create client object */

            msg.InnerHtml = "Thank You ! Your Booking is Sucessful!";
            string fname = txtfname.Text.ToString();
            string lname = txtlname.Text.ToString();
            string address = txtstreetadd.Text.ToString();
            string city = txtcity.Text.ToString();
            string pscode = txtpscode.Text.ToString();
            string countryname = country.Text.ToString();
            string email = txtemail.Text.ToString();
            string phone = txtphone.Text.ToString();

            Client newclient = new Client();
            newclient.ClientFirstName = fname;
            newclient.ClientLastName = lname;
            newclient.ClientStreetAddress = address;
            newclient.ClientCity = city;
            newclient.ClientPSCode = pscode;
            newclient.ClientCountryName = countryname;
            newclient.ClientEmail = email;
            newclient.ClientPhone = phone;




            /*Booking Info object */ 

            int noPerson = int.Parse(txtperson.Text);
      
            DateTime checkinDate = Convert.ToDateTime(txtcheckin.Text);
                                                                           //Reference
            DateTime checkoutDate = Convert.ToDateTime(txtcheckout.Text); //https://stackoverflow.com/questions/45082491/how-to-get-datepicker-value-in-code-behind-in-asp-net-webforms-on-button-click

            Booking newbooking = new Booking(noPerson, checkinDate, checkoutDate);



         




            /* Property object*/

            string roomType = property_qualityDelux.SelectedItem.Value.ToString();

            string propertyType = property_quality.SelectedItem.Value.ToString();

            List<String> facility = new List<string> { "Free WiFi", "Parking" };

            Property newproperty = new Property(roomType, propertyType, facility);

            List<string> facilities = new List<string>();
            foreach(Control control in extraAddFacility.Controls)
            {
                if(control.GetType() == typeof(CheckBox))
                {
                    CheckBox cbfacilities = (CheckBox)control;

                    if(cbfacilities.Checked)
                    {
                        facility.Add(cbfacilities.Text);
                    }

                }
            }

            newproperty.facility = newproperty.facility.Concat(facilities).ToList();









            Invoice newinvoice = new Invoice(newclient, newbooking, newproperty);

            InvoiceInfo.InnerHtml = newinvoice.GenerateInvoice();
           

        }

    }
}
